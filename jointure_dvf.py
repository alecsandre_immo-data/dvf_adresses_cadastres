import os
from datetime import datetime
import requests
from threading import Thread
import pandas as pd
import geopandas as gpd
from os import listdir
import numpy as np
import traceback

# variables (colonnes à garder, leurs noms, les chemins de fichiers etc...)

df_manquants = pd.DataFrame()
date_last_update = '2022-01-01'
master_df_drop = ['geometry','latitude_x','longitude_x','planNumber_y','streetName_y','streetNumber_y',
                'section_y','prefix_y','inseeCode_y','streetSuffix_y','postCode_y','cityName_y','id_parcelle','streetType_x']
nom_colonnes_master_df = {'prefix_x': 'prefix',
                            'section_x': 'section',
                            'planNumber_x': 'planNumber',
                            'streetNumber_x': 'streetNumber',
                            'streetName_x': 'streetName',
                            'postCode_x': 'postCode',
                            'longitude_y': 'longitude',
                            'latitude_y': 'latitude',
                            'streetSuffix_x' : 'streetSuffix',
                            'cityName_x' :'cityName',
                            'inseeCode_x': 'inseeCode',
                            'streetType_y' : 'streetType'}
colonnes_cadastre = ['geometry', 'id','commune','prefixe','section','numero']
noms_colonnes_cadastre = {'id' :'parcelId','commune': 'inseeCode','prefixe' : 'prefix','numero' :'planNumber'}
liste_espaces = ['  ', '   ', '    ', '     ','      ','       ']
dico_types = {"ALL":"Allee","AV":"Avenue","BD":"Boulevard","CAR":"Carrefour", "CHE":"Chemin",
            "CHS":"Chaussee","CITE":"Cite","COR":"Corniche","CRS":"Cours", "DOM":"Domaine","DSC":"Descente",
            "ECA":"Ecart","ESP":"Esplanade","FG":"Faubourg", "GR":"Grande Rue","HAM":"Hameau","HLE":"Halle",
            "IMP":"Impasse","LD":"Lieu-dit",  "LOT":"Lotissement","MAR":"Marche", "MTE":"Montee",
            "PAS":"Passage","PL":"Place", "PLN":"Plaine","PLT":"Plateau", "PRO":"Promenade", "PRV":"Parvis",
            "PTR":"petite rue","QUA":"Quartier", "QUAI":"Quai","RES":"Residence", "RLE":"Ruelle",
            "ROC":"Rocade", "RPT":"Rond-point",  "RTE":"Route","RUE":"Rue", "SEN":"Sente","SEN":"Sentier",
            "SQ":"Square", "TPL":"Terre-plein", "TRA":"Traverse", "VLA":"Villa","VLGE":"Village"}

# lancement du script normal
"""
#chemins de fichiers
path_jour = "D:/immo-data/jeux_donnees/niveau_commune/" + str(datetime.date(datetime.now()))
path_cadastres = "D:/immo-data/jeux_donnees/niveau_commune/"+ str(datetime.date(datetime.now())) + "/cadastres"
path_adresses = "D:/immo-data/jeux_donnees/niveau_commune/"+ str(datetime.date(datetime.now())) + "/adresses"
path_dvf = "D:/immo-data/jeux_donnees/niveau_commune/"+ str(datetime.date(datetime.now())) + "/dvf"


os.chdir(path_jour)
folder_master= 'master'
os.makedirs(folder_master)
path_master = path_jour + '/' + folder_master

path_master = "D:/immo-data/jeux_donnees/niveau_commune/"+ str(datetime.date(datetime.now())) + "/master"
"""

# lancement temporaire temporaire pour tests
jour = "2022-04-11"
path_jour = "D:/immo-data/jeux_donnees/niveau_commune/" + jour
path_cadastres = "D:/immo-data/jeux_donnees/niveau_commune/"+ jour + "/cadastres"
path_adresses = "D:/immo-data/jeux_donnees/niveau_commune/"+ jour + "/adresses"
path_dvf = "D:/immo-data/jeux_donnees/niveau_commune/"+ jour + "/dvf"


os.chdir(path_jour)
folder_master= 'master_dvf'
os.makedirs(folder_master)
path_master = path_jour + '/' + folder_master
#fin temporaire




#fonction pour créer liste de fichiers
def find_csv_filenames(path_to_dir, suffix=".csv" ):
    filenames = listdir(path_to_dir)
    return [ filename for filename in filenames if filename.endswith( suffix ) ]

liste_cadastres = find_csv_filenames(path_cadastres)

liste_dvf = find_csv_filenames(path_dvf)

liste_adresses = find_csv_filenames(path_adresses)

# fonction de jointure des fichiers si dans les listes
def jointure(fichier):   

    try :

        # import du cadastre 
        cadastre = 'https://cadastre.data.gouv.fr/data/etalab-cadastre/' + date_last_update + '/geojson/communes/' + debut_fichier +'/'+ numero_fichier +'/cadastre-'+ numero_fichier +'-parcelles.json.gz'
        df_cadastre = pd.read_json(cadastre, 
                                orient='str',
                                compression='gzip')
        df_cadastre = df_cadastre["features"]
        df_cadastre = gpd.GeoDataFrame.from_features(df_cadastre)
        df_cadastre = df_cadastre[colonnes_cadastre]
        df_cadastre.rename(noms_colonnes_cadastre,axis=1,inplace=True)

        # import de l'adresse
        adresse = path_adresses + "/" + fichier
        df_adresse = pd.read_csv(adresse)
        df_adresse.drop('geometry_duplicate', axis =1, inplace = True)
        df_adresse = gpd.GeoDataFrame(df_adresse, geometry= gpd.points_from_xy(df_adresse.longitude, df_adresse.latitude))
        df_adresse.replace(liste_espaces,' ', inplace=True)
        df_adresse['streetName'] = df_adresse['streetName'].str.normalize('NFKD').str.encode('ascii', errors='ignore').str.decode('utf-8')
        df_adresse['streetSuffix'] = df_adresse['streetSuffix'].str.upper()
        df_adresse['cityName'] = df_adresse['cityName'].str.upper()
        df_adresse['cityName'] = df_adresse['cityName'].str.normalize('NFKD').str.encode('ascii', errors='ignore').str.decode('utf-8')
        df_adresse['streetNumber'] = df_adresse['streetNumber'].apply(str)
        df_adresse['streetNumber'] = df_adresse['streetNumber'].str.replace('.0', '')
        #df_adresse['streetType'] = df_adresse['streetName'].str.split(' ').str[0]
        df_adresse['streetType'] = df_adresse['streetName'].apply(lambda x: x.split(' ')[0] if ' ' in x else np.nan)
        #df_adresse['streetName_sans_type'] = df_adresse['streetName'].str.split(n=1).str[1]
        df_adresse['postCode'] = df_adresse['postCode'].apply(str)
        df_adresse['postCode'] = df_adresse['postCode'].str.replace('.0', '')

        # import des transaction
        dvf = path_dvf + "/" + fichier
        df_dvf = pd.read_csv(dvf)
        df_dvf.replace(liste_espaces,' ', inplace=True)
        df_dvf['streetSuffix'] = df_dvf['streetSuffix'].apply(str)
        df_dvf['streetType'] = df_dvf['streetType'].replace(dico_types).str.upper()
        df_dvf['streetName'] = df_dvf['streetName'].str.normalize('NFKD').str.encode('ascii', errors='ignore').str.decode('utf-8')
        df_dvf['streetNumber'] = df_dvf['streetNumber'].apply(str)
        df_dvf['streetNumber'] = df_dvf['streetNumber'].str.replace('.0', '')
        df_dvf['postCode'] = df_dvf['postCode'].apply(str)
        df_dvf['postCode'] = df_dvf['postCode'].str.replace('.0', '')
        df_dvf['streetName'] = df_dvf['streetType'] + ' ' + df_dvf['streetName']

        # jointure sjoin cadastres-adresses
        polygons_contains = df_cadastre.sjoin(df_adresse, how="left", predicate='contains')
        polygons_contains.drop(['inseeCode_right','index_right'], axis= 1, inplace = True)
        polygons_contains.rename({'inseeCode_left': 'inseeCode'},axis=1, inplace=True)

        # jointure sur adresse postale
        merged_left = pd.merge(df_dvf, df_adresse, on=['streetNumber', 'streetName', 'inseeCode', 'cityName','streetSuffix','streetType'], how="left")
        merged_left.drop(['geometry','postCode_y'],axis = 1, inplace = True)
        merged_left.rename({'streetSuffix_x': 'streetSuffix','inseeCode_x' : 'inseeCode','cityName_x' : 'cityName','postCode_x': 'postCode'}, axis=1, inplace=True)

        # jointure des deux derniers DF créés
        master_df = pd.merge(merged_left, polygons_contains, left_on='id_parcelle', right_on='parcelId', how="left")
        master_df.rename({'geometry_y': 'geometry'},axis=1, inplace=True)
        master_df = master_df.set_geometry(col='geometry')
        master_df['longitude_y'] = np.where(master_df['longitude_y'].isna(), master_df['geometry'].centroid.y, master_df['longitude_y'] )
        master_df['latitude_y'] = np.where(master_df['latitude_y'].isna(), master_df['geometry'].centroid.x, master_df['latitude_y'] )
        master_df.rename(nom_colonnes_master_df,axis=1, inplace=True)
        master_df['streetType'] = master_df['streetType'].fillna(master_df["streetType_x"])
        master_df.drop(master_df_drop,axis = 1,  inplace = True)

        path_dvf_master = path_master  + '/dvf_master' + numero_fichier + '.csv'
        master_df.to_csv(path_dvf_master,index = False)


        # ajout des lignes n'ayant pas de parcelle dans un DF pour exploration
        df_tampon_manquant = master_df[master_df['parcelId'].isnull()]
        global df_manquants
        frames = [df_manquants, df_tampon_manquant]
        df_manquants = pd.concat(frames)


        print(f'ok')

    except Exception:
         print(f"\nN'a pas fonctionné car : \n{traceback.format_exc()}\n")

for i in range(1000,1100):
    
    if i < 10000:
        debut_fichier = '0' + str(i)[0]
        nom_fichier = str(i) + '.csv'
        numero_fichier = '0' + str(i)
        if nom_fichier in liste_cadastres and nom_fichier in liste_dvf and nom_fichier in liste_adresses:
            jointure(nom_fichier)
    
    else:
        debut_fichier = str(i)[0:2]
        nom_fichier = str(i) + '.csv'
        numero_fichier = str(i)
        if nom_fichier in liste_cadastres and nom_fichier in liste_dvf and nom_fichier in liste_adresses:
            jointure(nom_fichier)


for i in range(1000,99000):
    
    if i < 10000:
        debut_fichier = '0' + str(i)[0]
        nom_fichier = str(i) + '.csv'
        numero_fichier = '0' + str(i)
        if nom_fichier in liste_cadastres and nom_fichier in liste_dvf and nom_fichier in liste_adresses:
            jointure(nom_fichier)
    
    else:
        debut_fichier = str(i)[0:2]
        nom_fichier = str(i) + '.csv'
        numero_fichier = str(i)
        if nom_fichier in liste_cadastres and nom_fichier in liste_dvf and nom_fichier in liste_adresses:
            jointure(nom_fichier)


for i in range(0,1000):
    
    if i < 10:
        debut_fichier = '2B'
        nom_fichier = '2B' + '00' + str(i) + '.csv'
        numero_fichier = '2B00' + str(i)
        if nom_fichier in liste_cadastres and nom_fichier in liste_dvf and nom_fichier in liste_adresses:
            jointure(nom_fichier)
    
    elif i > 9 and i < 100:
        debut_fichier = '2B'
        nom_fichier = '2B' + '0' + str(i) + '.csv'
        numero_fichier = '2B0' + str(i)
        if nom_fichier in liste_cadastres and nom_fichier in liste_dvf and nom_fichier in liste_adresses:
            jointure(nom_fichier)

    elif i > 99 :
        debut_fichier = '2B'
        nom_fichier = '2B' + str(i) + '.csv'
        numero_fichier = '2B' + str(i)
        if nom_fichier in liste_cadastres and nom_fichier in liste_dvf and nom_fichier in liste_adresses:
            jointure(nom_fichier)

for i in range(0,1000):
    
    if i < 10:
        debut_fichier = '2A'
        nom_fichier = '2A' + '00' + str(i) + '.csv'
        numero_fichier = '2A00' + str(i)
        if nom_fichier in liste_cadastres and nom_fichier in liste_dvf and nom_fichier in liste_adresses:
            jointure(nom_fichier)
    
    elif i > 9 and i < 100:
        debut_fichier = '2A'
        nom_fichier = '2A' + '0' + str(i) + '.csv'
        numero_fichier = '2A0' + str(i)
        if nom_fichier in liste_cadastres and nom_fichier in liste_dvf and nom_fichier in liste_adresses:
            jointure(nom_fichier)

    elif i > 99 :
        debut_fichier = '2A'
        nom_fichier = '2A' + str(i) + '.csv'
        numero_fichier = '2A' + str(i)
        if nom_fichier in liste_cadastres and nom_fichier in liste_dvf and nom_fichier in liste_adresses:
            jointure(nom_fichier)


path_manquant = path_jour + '/parcelId_manquant_dvf.csv'
df_manquants.to_csv(path_manquant, index= False)

# cd D:/immo-data/code
# python jointure_commune_19_04.py