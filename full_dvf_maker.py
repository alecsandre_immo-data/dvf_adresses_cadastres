import os
from datetime import datetime
import requests
from threading import Thread
import pandas as pd
import geopandas as gpd
from os import listdir
import numpy as np
import traceback
import time
from datetime import timedelta
start_time = time.monotonic()   


path_dvf = "D:/immo-data/jeux_donnees/2022-05-10/master_dvf_test"
df_parcelId = pd.read_csv("D:/immo-data/jeux_donnees/2022-05-10/parcelId_manquant_dvf.csv")

#fonction pour créer les listes de fichiers
def find_csv_filenames(path_to_dir, suffix ):
    filenames = listdir(path_to_dir)
    return [ filename for filename in filenames if filename.endswith( suffix ) ]
liste_dvf = find_csv_filenames(path_dvf,"csv")

df_master = pd.DataFrame()

for i in liste_dvf:
    df = pd.read_csv(path_dvf + "/" + str(i))
    
    frames = [df_master, df]
    df_master = pd.concat(frames)
    print(f"{str(i)} is added")


frames = [df_master, df_parcelId]
df_master = pd.concat(frames)
df_master['geometry_flag'] = df_master['geometry'].astype(str)
df_master['geometry_flag'] = df_master['geometry_flag'].apply(lambda x: True if x.split(' ')[0] == 'POLYGON' else False)
df_master.drop("geometry", axis = 1, inplace = True)
df_master.to_csv("D:/immo-data/jeux_donnees/2022-05-10/full_file_for_stats.csv")


end_time = time.monotonic()
print(timedelta(seconds=end_time - start_time))