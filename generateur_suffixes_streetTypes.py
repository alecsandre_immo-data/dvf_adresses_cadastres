import os
from datetime import datetime
import requests
from threading import Thread
import pandas as pd
import geopandas as gpd
from os import listdir
import numpy as np
import traceback

# chemins et variables

path_dvf = 'D:/immo-data/jeux_donnees/2022-04-14/dvf'
path_adresses_extraites = 'D:/immo-data/jeux_donnees/2022-04-14/adresses_extraites'
path_adresses = 'D:/immo-data/jeux_donnees/2022-04-14/adresses'
path_values = 'D:/immo-data/jeux_donnees/values'

total_values = pd.DataFrame()

def find_csv_filenames(path_to_dir, suffix):
    filenames = listdir(path_to_dir)
    return [ filename for filename in filenames if filename.endswith( suffix ) ]

dvf = find_csv_filenames(path_dvf, ".csv")
print("dvf listed")
adresses = find_csv_filenames(path_adresses, ".gz")
print("adresse listed")
adresses_extraites = find_csv_filenames(path_adresses_extraites, ".gz")
print("adresses_extraites listed")


def valuing(liste_a_traiter, colonne_a_traiter, path_df, nom_fichier, nom_valeur):
    for i in liste_a_traiter:
        path = path_df + '/' + i
        df= pd.read_csv(path,low_memory=False)
        df[colonne_a_traiter] = df[colonne_a_traiter].str.upper()
        df[colonne_a_traiter] = df[colonne_a_traiter].str.normalize('NFKD').str.encode('ascii', errors='ignore').str.decode('utf-8')
        df[nom_valeur] = df[colonne_a_traiter]
        df_value = df[nom_valeur].value_counts(dropna=False)
        df_value = df_value.reset_index()
        df_value.columns = ['uniqueValues', 'counts_for_' + nom_valeur]

        global total_values
        frames = [total_values, df_value]
        total_values = pd.concat(frames)

    total_values = total_values.groupby('uniqueValues', as_index=False).sum()
    total_values.to_csv(path_values + "/"+ nom_fichier + "_" + nom_valeur+ "_values.csv", index = False)
    print(f"\n{nom_fichier}_{nom_valeur} généré")  
    total_values = pd.DataFrame()


def valuing_gz(liste_a_traiter, colonne_a_traiter, path_df, nom_fichier, nom_valeur, split_yes_no):
    for i in liste_a_traiter:
        path = path_df + '/' + i
        df= pd.read_csv(path, compression='gzip', sep=";", low_memory=False)
        df[colonne_a_traiter] = df[colonne_a_traiter].str.upper()
        df[colonne_a_traiter] = df[colonne_a_traiter].str.normalize('NFKD').str.encode('ascii', errors='ignore').str.decode('utf-8')

        if split_yes_no == 'yes':
            df[nom_valeur] = df[colonne_a_traiter].str.split(' ').str[0]  
            df_value = df[nom_valeur].value_counts(dropna=False)
            df_value = df_value.reset_index()
            df_value.columns = ['uniqueValues', 'counts_for_' + nom_valeur]
             
        else :
            df[nom_valeur] = df[colonne_a_traiter]
            df_value = df[nom_valeur].value_counts(dropna=False)
            df_value = df_value.reset_index()
            df_value.columns = ['uniqueValues', 'counts_for_' + nom_valeur]

        global total_values
        frames = [total_values, df_value]
        total_values = pd.concat(frames)

    total_values = total_values.groupby('uniqueValues', as_index=False).sum()
    total_values.to_csv(path_values + "/"+ nom_fichier + "_" + nom_valeur+ "_values.csv", index = False)
    print(f"\n{nom_fichier}_{nom_valeur} généré")  
    total_values = pd.DataFrame()


valuing(dvf,'B/T/Q',path_dvf,'dvf','suffixe')
if total_values.empty:
    print('total_values is empty')

valuing(dvf,'Type de voie',path_dvf,'dvf','streetType')
if total_values.empty:
    print('total_values is empty')

valuing_gz(adresses,'rep',path_adresses,'adresses','suffix', 'no')
if total_values.empty:
    print('total_values is empty')

valuing_gz(adresses_extraites,'suffixe', path_adresses_extraites,'adresses_extraites','suffixe', 'no')
if total_values.empty:
    print('total_values is empty')

valuing_gz(adresses_extraites,'voie_nom',path_adresses_extraites,'adresses_extraites','streetType', 'yes')
if total_values.empty:
    print('total_values is empty')


valuing_gz(adresses,'nom_voie',path_adresses,'adresses','streetType', 'yes')
if total_values.empty:
    print('total_values is empty')


# cd D:/immo-data/code
# python generateur_de_types.py