import geopandas as gpd
import pandas as pd 
import os
from datetime import datetime
import numpy as np


path = 'D:/immo-data/jeux_donnees/niveau_commune'

#creation du folder jour

os.chdir(path)
folder_jour= str(datetime.date(datetime.now()))
#os.makedirs(folder_jour)

path_jour = path + '/'+folder_jour

os.chdir(path_jour)

folder_adresses='adresses'
#os.makedirs(folder_adresses)
folder_cadastre='cadastres'
#os.makedirs(folder_cadastre)
folder_dvf= 'dvf'
#os.makedirs(folder_dvf)

path_adresses = path_jour + '/' + folder_adresses
path_cadastre = path_jour + '/' + folder_cadastre
path_dvf = path_jour + '/' + folder_dvf

path_jour_dl = 'D:/immo-data/jeux_donnees/' + str(datetime.date(datetime.now()))
path_dl_adresses = path_jour_dl + '/adresses/adresses-'
path_dl_cadastre = path_jour_dl + '/cadastres/cadastre-'
path_dl_dvf = path_jour_dl + '/dvf/'

path_adresses = path_jour + '/adresses/'
path_cadastre = path_jour + '/cadastres/'
path_dvf = path_jour + '/dvf/'

departements_sans_dvf = [20,57,67,68,975]

colonnes_cadastre = ['geometry','id','commune','prefixe','section','numero']
noms_colonnes_cadastre = {'id' :'parcelId',
                            'commune': 'inseeCode',
                            'prefixe' : 'prefix',
                            'numero' :'planNumber'}

colonnes_adresse = ['numero','rep','nom_voie','code_postal','code_insee','nom_commune','lon','lat']
noms_colonnes_adresse =  {'numero': 'streetNumber',
                            'rep' :'streetSuffix',
                            'code_postal': 'postCode',
                            'nom_voie' : 'streetName',
                            'code_insee' :'inseeCode',
                            'nom_commune' :'cityName',
                            'lon' :'longitude',
                            'lat' :'latitude'}        

colonnes_dvf = ['Date mutation','Nature mutation','Valeur fonciere','No voie','B/T/Q','Type de voie',
           'Voie','Code postal','Commune','Code departement','codeDepartement_codeCommuneSeule','Prefixe de section',
           'Section','No plan','Code type local','Surface reelle bati','Nombre pieces principales',
           'Nature culture','Surface terrain','id_parcelle','idMutation']
noms_colonnes_dvf = {'Date mutation' :'txDate',
                        'Nature mutation': 'txType',
                        'Valeur fonciere': 'price',
                        'No voie': 'streetNumber',
                        'B/T/Q': 'streetSuffix',
                        'Type de voie': 'streetType',
                        'Voie': 'streetName',
                        'Code postal': 'postCode',
                        'Commune': 'cityName',
                        'Code departement' : 'departmentCode',
                        'codeDepartement_codeCommuneSeule' : 'inseeCode',
                        'Prefixe de section' :'prefix',
                        'Section': 'section',
                        'No plan': 'planNumber',
                        'Code type local': 'realtyType',
                        'Surface reelle bati': 'livingArea',
                        'Nombre pieces principales': 'rooms',
                        'Nature culture': 'landType',
                        'Surface terrain': 'landArea'}

master_df_drop = ['geometry_duplicate_y',
                            'geometry_y',
                            'latitude_y',
                            'longitude_y',
                            'planNumber_y',
                            'section_y',
                            'prefix_y',
                            'postCode_y',
                            'streetName_y',
                            'streetNumber_y',
                            'streetSuffix_y','cityName_y',
                            'inseeCode_y']

nom_colonnes_master_df = {'geometry_x' :'geometry',
                            'prefix_x': 'prefix',
                            'section_x': 'section',
                            'planNumber_x': 'planNumber',
                            'streetNumber_x': 'streetNumber',
                            'streetName_x': 'streetName',
                            'postCode_x': 'postCode',
                            'longitude_x': 'longitude',
                            'latitude_x': 'latitude',
                            'geometry_duplicate_x' : 'geometry_duplicate',
                            'streetSuffix_x' : 'streetSuffix',
                            'cityName_x' :'cityName',
                            'inseeCode_x': 'inseeCode'}


def communing(min_range, max_range):
    for i in range(min_range,max_range):
        #if i != 20 and i != 975:
        if i not in departements_sans_dvf: 
            
            
   # import et traitement du cadastre
            cadastre = pd.read_json(path_dl_cadastre + str(i) + "-parcelles.json.gz", 
                                    orient='str',
                                    compression='gzip')
            cadastrePolygons = cadastre["features"]
            cadastre = gpd.GeoDataFrame.from_features(cadastrePolygons)

            cadastre = cadastre[colonnes_cadastre]
            cadastre.rename(noms_colonnes_cadastre,
                            axis=1,
                            inplace=True)
                
            #creation df_cadastre au niveau commune     
            cadastre['inseeCode'] = cadastre['inseeCode'].astype(int)
            minimale_cadastre = cadastre['inseeCode'].min() 
            maximale_cadastre = cadastre['inseeCode'].max() + 1
            for y in range(minimale_cadastre, maximale_cadastre):

                cadastre_tampon = cadastre[cadastre['inseeCode'] == y]
                path_sortie_cadastre = path_cadastre + str(y) + '.csv'
                cadastre_tampon.to_csv(path_sortie_cadastre, index = False)


            # import et traitement des adresses
            adresse = pd.read_csv(path_dl_adresses + str(i) +".csv.gz",
                             compression='gzip', sep=";")
            
            adresse = adresse[colonnes_adresse]
            adresse['numero'] = adresse['numero'].astype(float)
            adresse['code_postal'] = adresse['code_postal'].astype(float)
            adresse['nom_voie'] = adresse['nom_voie'].str.upper()
            adresse = gpd.GeoDataFrame(adresse, geometry= gpd.points_from_xy(adresse.lon, adresse.lat))
            adresse['geometry_duplicate'] = adresse['geometry']
            adresse.rename(noms_colonnes_adresse,
                            axis=1,
                            inplace=True)
            
            #creation df_adresse au niveau commune 
            minimale_adresse = adresse['inseeCode'].min() 
            maximale_adresse = adresse['inseeCode'].max() + 1
            for y in range(minimale_adresse, maximale_adresse):
                adresse_tampon = adresse[adresse['inseeCode'] == y]
                path_sortie_adresse = path_adresses + str(y) + '.csv'
                adresse_tampon.to_csv(path_sortie_adresse, index = False)

            # import et traitement du dvf
            dvf = pd.read_csv(path_dl_dvf + 'dvf' + str(i) + '.csv')

            dvf = dvf[colonnes_dvf]
            dvf.rename(noms_colonnes_dvf,
                        axis=1,
                        inplace=True)

            #creation df_dvf au niveau commune
            minimale_dvf = dvf['inseeCode'].min() 
            maximale_dvf = dvf['inseeCode'].max() + 1
            for y in range(minimale_dvf, maximale_dvf):
                dvf_tampon = dvf[dvf['inseeCode'] == y]
                path_sortie_dvf = path_dvf + str(y) + '.csv'
                dvf_tampon.to_csv(path_sortie_dvf, index = False)









def communing_corse(departement_corse):
    cadastre = pd.read_json(path_dl_cadastre + departement_corse + "-parcelles.json.gz", 
                                    orient='str',
                                    compression='gzip')
    cadastrePolygons = cadastre["features"]
    cadastre = gpd.GeoDataFrame.from_features(cadastrePolygons)

    cadastre = cadastre[colonnes_cadastre]
    cadastre.rename(noms_colonnes_cadastre,
                    axis=1,
                    inplace=True)
                
    #creation df_cadastre au niveau commune     
    cadastre['inseeCode'] = cadastre['inseeCode'].str.replace(departement_corse, '')
    cadastre['inseeCode'] = cadastre['inseeCode'].astype(int)
    minimale = cadastre['inseeCode'].min() 
    maximale = cadastre['inseeCode'].max() + 1
    for i in range(minimale, maximale):
        cadastre_tampon = cadastre[cadastre['inseeCode'] == i]
        cadastre_tampon['inseeCode'] = cadastre_tampon['inseeCode'].astype(str)
        cadastre_tampon['inseeCode'] = cadastre_tampon['inseeCode'].str.pad(width=3, side='left', fillchar='0')
        cadastre_tampon['inseeCode'] = cadastre_tampon['inseeCode'].str.pad(width=4, side='left', fillchar='A')
        cadastre_tampon['inseeCode'] = cadastre_tampon['inseeCode'].str.pad(width=5, side='left', fillchar='2')
        if i < 10 :
        
            path_corse = path_cadastre + departement_corse + '00' + str(i) + '.csv'
            cadastre_tampon.to_csv(path_corse, index = False)
        
        elif i < 100 and i > 9 :
            
            path_corse = path_cadastre + departement_corse + '0' + str(i) + '.csv'
            cadastre_tampon.to_csv(path_corse, index = False)
        
        elif i >= 100:
            
            path_corse = path_cadastre + departement_corse + str(i) + '.csv'
            cadastre_tampon.to_csv(path_corse, index = False)

    # import et traitement des adresses
    adresse = pd.read_csv(path_dl_adresses + departement_corse +".csv.gz"
                    ,compression='gzip', sep=";")
    
    adresse = adresse[colonnes_adresse]
    adresse['numero'] = adresse['numero'].astype(float)
    adresse['code_postal'] = adresse['code_postal'].astype(float)
    adresse['nom_voie'] = adresse['nom_voie'].str.upper()
    adresse = gpd.GeoDataFrame(adresse, geometry= gpd.points_from_xy(adresse.lon, adresse.lat))
    adresse['geometry_duplicate'] = adresse['geometry']
    adresse.rename(noms_colonnes_adresse,
                    axis=1,
                    inplace=True)
    
    #creation df_adresse au niveau commune 

    adresse['inseeCode'] = adresse['inseeCode'].str.replace(departement_corse, '')
    adresse['inseeCode'] = adresse['inseeCode'].astype(int)
    minimale = adresse['inseeCode'].min() 
    maximale = adresse['inseeCode'].max() + 1
    for i in range(minimale, maximale):
        adresse_tampon = adresse[adresse['inseeCode'] == i]
        adresse_tampon['inseeCode'] = adresse_tampon['inseeCode'].astype(str)
        adresse_tampon['inseeCode'] = adresse_tampon['inseeCode'].str.pad(width=3, side='left', fillchar='0')
        adresse_tampon['inseeCode'] = adresse_tampon['inseeCode'].str.pad(width=4, side='left', fillchar='A')
        adresse_tampon['inseeCode'] = adresse_tampon['inseeCode'].str.pad(width=5, side='left', fillchar='2')
        
        if i < 10 :
            
            path_corse = path_adresses + departement_corse +'00' + str(i) + '.csv'
            adresse_tampon.to_csv(path_corse, index = False)
        
        elif i < 100 and i > 9 :
            
            path_corse = path_adresses + departement_corse +'0' + str(i) + '.csv'
            adresse_tampon.to_csv(path_corse, index = False)
        
        elif i >= 100:
            
            path_corse = path_adresses + departement_corse + str(i) + '.csv'
            adresse_tampon.to_csv(path_corse, index = False)

    # import et traitement du dvf
    dvf = pd.read_csv(path_dl_dvf + 'dvf' + departement_corse + '.csv')

    dvf = dvf[colonnes_dvf]
    dvf.rename(noms_colonnes_dvf,
                axis=1,
                inplace=True)

    #creation df_dvf au niveau commune

    dvf['inseeCode'] = dvf['inseeCode'].str.replace(departement_corse, '')
    dvf['inseeCode'] = dvf['inseeCode'].astype(int)
    minimale = dvf['inseeCode'].min() 
    maximale = dvf['inseeCode'].max() + 1
    for i in range(minimale, maximale):
        dvf_tampon = dvf[dvf['inseeCode'] == i]
        dvf_tampon['inseeCode'] = dvf_tampon['inseeCode'].astype(str)
        dvf_tampon['inseeCode'] = dvf_tampon['inseeCode'].str.pad(width=3, side='left', fillchar='0')
        dvf_tampon['inseeCode'] = dvf_tampon['inseeCode'].str.pad(width=4, side='left', fillchar='A')
        dvf_tampon['inseeCode'] = dvf_tampon['inseeCode'].str.pad(width=5, side='left', fillchar='2')
        
        if i < 10 :
            
            path_corse = path_dvf + departement_corse + '00' + str(i) + '.csv'
            dvf_tampon.to_csv(path_corse, index = False)
        
        elif i < 100 and i > 9 :
            
            path_corse = path_dvf + departement_corse + '0' + str(i) + '.csv'
            dvf_tampon.to_csv(path_corse, index = False)
        
        elif i >= 100:
            
            path_corse = path_dvf + departement_corse + str(i) + '.csv'
            dvf_tampon.to_csv(path_corse, index = False)

def suppresseur(dossier, taille_minimum):
    for root, _, files in os.walk('D:/immo-data/jeux_donnees/niveau_commune/' + str(datetime.date(datetime.now())) + '/' + dossier):
        for f in files:
            fullpath = os.path.join(root, f)
            try:
                if os.path.getsize(fullpath) < 1 * taille_minimum:   #set file size in kb
                    print(fullpath)
                    os.remove(fullpath)
            except WindowsError:
                print("Error" + fullpath)
'''
communing(1,97)
communing(971,976)
communing_corse('2A')
communing_corse('2B')

suppresseur('dvf', 100)
suppresseur('cadastres', 100)
suppresseur('adresses', 100)


communing(58, 97)
communing(971,976)
'''
communing_corse('2A')
communing_corse('2B')

# cd D:\immo-data\code
# python communing_sans_nom_04_04.py