import os
from datetime import datetime
import requests
from threading import Thread
import pandas as pd
import geopandas as gpd
from os import listdir
import numpy as np
import traceback
import math
import statistics

# variables (colonnes à garder, leurs noms, les chemins de fichiers etc...)

date_last_update = '2022-01-01'
path_dvf = "D:/immo-data/jeux_donnees/niveau_commune/2022-04-11/dvf"
path_adresses = "D:/immo-data/jeux_donnees/niveau_commune/2022-04-11/adresses"
path_cadastres = "D:/immo-data/jeux_donnees/niveau_commune/2022-04-11/cadastres"
colonnes_cadastre = ['geometry', 'id','commune','prefixe','section','numero']
noms_colonnes_cadastre = {'id' :'parcelId','commune': 'inseeCode','prefixe' : 'prefix','numero' :'planNumber'}
liste_espaces = ['  ', '   ', '    ', '     ','      ','       ']
master_df_drop = ['geometry','latitude_x','longitude_x','planNumber_y','streetName_y','streetNumber_y',
                'section_y','prefix_y','inseeCode_y','streetSuffix_y','postCode_y','cityName_y','id_parcelle']
nom_colonnes_df_final = {'planNumber_x': 'planNumber',
                            'streetNumber_x': 'streetNumber',
                            'streetName_x': 'streetName',
                            'postCode_x': 'postCode',
                            'longitude_y': 'longitude',
                            'latitude_y': 'latitude',
                            'streetSuffix_x' : 'streetSuffix',
                            'cityName_x' :'cityName',
                            'inseeCode_x': 'inseeCode',}
final_drop = ["idMutation","landArea","landType","rooms","livingArea","realtyType",
            "planNumber_y","section_y","prefix_y","departmentCode","price","txType",
            "txDate","id_parcelle","postCode_y","index_right",'prefix_x','section_x', 'planNumber']
dico_types = {"ALL":"Allee","AV":"Avenue","BD":"Boulevard","CAR":"Carrefour", "CHE":"Chemin",
            "CHS":"Chaussee","CITE":"Cite","COR":"Corniche","CRS":"Cours", "DOM":"Domaine","DSC":"Descente",
            "ECA":"Ecart","ESP":"Esplanade","FG":"Faubourg", "GR":"Grande Rue","HAM":"Hameau","HLE":"Halle",
            "IMP":"Impasse","LD":"Lieu-dit",  "LOT":"Lotissement","MAR":"Marche", "MTE":"Montee",
            "PAS":"Passage","PL":"Place", "PLN":"Plaine","PLT":"Plateau", "PRO":"Promenade", "PRV":"Parvis",
            "PTR":"petite rue","QUA":"Quartier", "QUAI":"Quai","RES":"Residence", "RLE":"Ruelle",
            "ROC":"Rocade", "RPT":"Rond-point",  "RTE":"Route","RUE":"Rue", "SEN":"Sente","SEN":"Sentier",
            "SQ":"Square", "TPL":"Terre-plein", "TRA":"Traverse", "VLA":"Villa","VLGE":"Village"}
liste_pourcentages = []
df_manquants = pd.DataFrame()

os.chdir("D:/immo-data/jeux_donnees/niveau_commune/2022-04-11")
folder_master= 'master_adresse_test'
#os.makedirs(folder_master)

#fonction pour créer liste de fichiers
def find_csv_filenames(path_to_dir, suffix=".csv" ):
    filenames = listdir(path_to_dir)
    return [ filename for filename in filenames if filename.endswith( suffix ) ]

liste_cadastres = find_csv_filenames(path_cadastres)
liste_dvf = find_csv_filenames(path_dvf)
liste_adresses = find_csv_filenames(path_adresses)



# fonction de jointure des fichiers si dans les listes
def jointure(fichier):   

    try : 

        # import du cadastre 
        lien_cadastre = 'https://cadastre.data.gouv.fr/data/etalab-cadastre/' + date_last_update + '/geojson/communes/' + debut_fichier +'/'+ numero_fichier +'/cadastre-'+ numero_fichier +'-parcelles.json.gz'
        df_cadastre = pd.read_json(lien_cadastre, 
                                orient='str',
                                compression='gzip')
        df_cadastre = df_cadastre["features"]
        df_cadastre = gpd.GeoDataFrame.from_features(df_cadastre)
        df_cadastre = df_cadastre[colonnes_cadastre]
        df_cadastre.rename(noms_colonnes_cadastre,axis=1,inplace=True)

        # import de l'adresse
        adresse = path_adresses + "/" + fichier
        df_adresse = pd.read_csv(adresse)
        df_adresse.drop('geometry_duplicate', axis =1, inplace = True)
        df_adresse = gpd.GeoDataFrame(df_adresse, geometry= gpd.points_from_xy(df_adresse.longitude, df_adresse.latitude))
        df_adresse.replace(liste_espaces,' ', inplace=True)
        df_adresse['streetName'] = df_adresse['streetName'].str.normalize('NFKD').str.encode('ascii', errors='ignore').str.decode('utf-8')
        df_adresse['streetSuffix'] = df_adresse['streetSuffix'].str.upper()
        df_adresse['cityName'] = df_adresse['cityName'].str.upper()
        df_adresse['cityName'] = df_adresse['cityName'].str.normalize('NFKD').str.encode('ascii', errors='ignore').str.decode('utf-8')
        df_adresse['streetNumber'] = df_adresse['streetNumber'].apply(str)
        df_adresse['streetNumber'] = df_adresse['streetNumber'].str.replace('.0', '')
        #df_adresse['streetType'] = df_adresse['streetName'].str.split(' ').str[0]
        df_adresse['streetType'] = df_adresse['streetName'].apply(lambda x: x.split(' ')[0] if ' ' in x else np.nan)
        #df_adresse['streetName_sans_type'] = df_adresse['streetName'].str.split(n=1).str[1]
        df_adresse['postCode'] = df_adresse['postCode'].apply(str)
        df_adresse['postCode'] = df_adresse['postCode'].str.replace('.0', '')

        # import des transaction
        dvf = path_dvf + "/" + fichier
        df_dvf = pd.read_csv(dvf)
        df_dvf.replace(liste_espaces,' ', inplace=True)
        df_dvf['streetType'] = df_dvf['streetType'].replace(dico_types).str.upper()
        df_dvf['streetName'] = df_dvf['streetName'].str.normalize('NFKD').str.encode('ascii', errors='ignore').str.decode('utf-8')
        df_dvf['streetNumber'] = df_dvf['streetNumber'].apply(str)
        df_dvf['streetNumber'] = df_dvf['streetNumber'].str.replace('.0', '')
        df_dvf['postCode'] = df_dvf['postCode'].apply(str)
        df_dvf['postCode'] = df_dvf['postCode'].str.replace('.0', '')
        df_dvf['streetName'] = df_dvf['streetType'] + ' ' + df_dvf['streetName']

        # jointure sjoin cadastres-adresses
        polygons_contains = df_adresse.sjoin(df_cadastre, how="left", predicate='within')
        polygons_contains['inseeCode_left'] = polygons_contains['inseeCode_left'].fillna(polygons_contains["inseeCode_right"])
        polygons_contains.drop(['geometry','inseeCode_right'], axis = 1, inplace = True)
        polygons_contains.rename({'inseeCode_left' : 'inseeCode'},axis= 1, inplace = True)   

        # jointure sur adresse postale
        df_final = pd.merge(polygons_contains, df_dvf, on=['streetNumber', 'streetName', 'inseeCode', 'cityName','streetSuffix','streetType'], how="left")   
        df_final['parcelId'] = df_final['parcelId'].fillna(df_final["id_parcelle"])
        df_final['postCode_x'] = df_final['postCode_x'].fillna(df_final["postCode_y"])
        df_final.rename(nom_colonnes_df_final,axis=1, inplace=True)
        df_final.drop(final_drop, axis = 1, inplace = True)


        path_df_final = "D:/immo-data/jeux_donnees/niveau_commune/2022-04-11/master_adresse_test/" + '/adresse_master' + numero_fichier + '.csv'
        df_final.to_csv(path_df_final, index= False)
        

        # ajout des lignes n'ayant pas de parcelle dans un DF pour exploration
        liste_pourcentages.append(round(df_final['parcelId'].isnull().mean()*100,2))
        df_tampon_manquant = df_final[df_final['parcelId'].isnull()]
        global df_manquants
        frames = [df_manquants, df_tampon_manquant]
        df_manquants = pd.concat(frames)

        print('ok')

    except Exception:
         print(f"\nN'a pas fonctionné car : \n{traceback.format_exc()} {fichier}\n")

for i in range(1000,1100):
    
    if i < 10000:
        debut_fichier = '0' + str(i)[0]
        nom_fichier = str(i) + '.csv'
        numero_fichier = '0' + str(i)
        if nom_fichier in liste_cadastres and nom_fichier in liste_dvf and nom_fichier in liste_adresses:
            jointure(nom_fichier)
    
    else:
        debut_fichier = str(i)[0:2]
        nom_fichier = str(i) + '.csv'
        numero_fichier = str(i)
        if nom_fichier in liste_cadastres and nom_fichier in liste_dvf and nom_fichier in liste_adresses:
            jointure(nom_fichier)
        
"""
for i in range(1000,99000):
    
    if i < 10000:
        debut_fichier = '0' + str(i)[0]
        nom_fichier = str(i) + '.csv'
        numero_fichier = '0' + str(i)
        if nom_fichier in liste_cadastres and nom_fichier in liste_dvf and nom_fichier in liste_adresses:
            jointure(nom_fichier)
    
    else:
        debut_fichier = str(i)[0:2]
        nom_fichier = str(i) + '.csv'
        numero_fichier = str(i)
        if nom_fichier in liste_cadastres and nom_fichier in liste_dvf and nom_fichier in liste_adresses:
            jointure(nom_fichier)


for i in range(0,1000):
    
    if i < 10:
        debut_fichier = '2B'
        nom_fichier = '2B' + '00' + str(i) + '.csv'
        numero_fichier = '2B00' + str(i)
        if nom_fichier in liste_cadastres and nom_fichier in liste_dvf and nom_fichier in liste_adresses:
            jointure(nom_fichier)
    
    elif i > 9 and i < 100:
        debut_fichier = '2B'
        nom_fichier = '2B' + '0' + str(i) + '.csv'
        numero_fichier = '2B0' + str(i)
        if nom_fichier in liste_cadastres and nom_fichier in liste_dvf and nom_fichier in liste_adresses:
            jointure(nom_fichier)

    elif i > 99 :
        debut_fichier = '2B'
        nom_fichier = '2B' + str(i) + '.csv'
        numero_fichier = '2B' + str(i)
        if nom_fichier in liste_cadastres and nom_fichier in liste_dvf and nom_fichier in liste_adresses:
            jointure(nom_fichier)

for i in range(0,1000):
    
    if i < 10:
        debut_fichier = '2A'
        nom_fichier = '2A' + '00' + str(i) + '.csv'
        numero_fichier = '2A00' + str(i)
        if nom_fichier in liste_cadastres and nom_fichier in liste_dvf and nom_fichier in liste_adresses:
            jointure(nom_fichier)
    
    elif i > 9 and i < 100:
        debut_fichier = '2A'
        nom_fichier = '2A' + '0' + str(i) + '.csv'
        numero_fichier = '2A0' + str(i)
        if nom_fichier in liste_cadastres and nom_fichier in liste_dvf and nom_fichier in liste_adresses:
            jointure(nom_fichier)

    elif i > 99 :
        debut_fichier = '2A'
        nom_fichier = '2A' + str(i) + '.csv'
        numero_fichier = '2A' + str(i)
        if nom_fichier in liste_cadastres and nom_fichier in liste_dvf and nom_fichier in liste_adresses:
            jointure(nom_fichier)


for i in range(1000,1100):
    
    if i < 10000:
        debut_fichier = '0' + str(i)[0]
        nom_fichier = str(i) + '.csv'
        numero_fichier = '0' + str(i)
        if nom_fichier in liste_cadastres and nom_fichier in liste_dvf and nom_fichier in liste_adresses:
            jointure(nom_fichier)
    
    else:
        debut_fichier = str(i)[0:2]
        nom_fichier = str(i) + '.csv'
        numero_fichier = str(i)
        if nom_fichier in liste_cadastres and nom_fichier in liste_dvf and nom_fichier in liste_adresses:
            jointure(nom_fichier)
"""
liste_pourcentages = [x for x in liste_pourcentages if math.isnan(x) == False]   
path_file = 'D:/immo-data/jeux_donnees/niveau_commune/2022-04-11/pourcentage_null.txt'

with open(path_file, 'w') as f:
    
        f.write(f"le pourcentage de null dans les DF est de {round(statistics.mean(liste_pourcentages),2)}%")
path_manquant = 'D:/immo-data/jeux_donnees/niveau_commune/2022-04-11/parcelId_manquant_adresse.csv'
df_manquants.to_csv(path_manquant, index= False)


# cd D:/immo-data/code/
# python adressing_2.py