import os
from datetime import datetime
import requests
import pandas as pd
import geopandas as gpd
from os import listdir
import numpy as np
import traceback

#variables et chemins
df_manquants = pd.DataFrame()
liste_espaces = ['  ', '   ', '    ', '     ','      ','       ']
dico_types = {"ALL":"Allee","AV":"Avenue","BD":"Boulevard","CAR":"Carrefour", "CHE":"Chemin",
            "CHS":"Chaussee","CITE":"Cite","COR":"Corniche","CRS":"Cours", "DOM":"Domaine","DSC":"Descente",
            "ECA":"Ecart","ESP":"Esplanade","FG":"Faubourg", "GR":"Grande Rue","HAM":"Hameau","HLE":"Halle",
            "IMP":"Impasse","LD":"Lieu-dit",  "LOT":"Lotissement","MAR":"Marche", "MTE":"Montee",
            "PAS":"Passage","PL":"Place", "PLN":"Plaine","PLT":"Plateau", "PRO":"Promenade", "PRV":"Parvis",
            "PTR":"petite rue","QUA":"Quartier", "QUAI":"Quai","RES":"Residence", "RLE":"Ruelle",
            "ROC":"Rocade", "RPT":"Rond-point",  "RTE":"Route","RUE":"Rue", "SEN":"Sente","SEN":"Sentier",
            "SQ":"Square", "TPL":"Terre-plein", "TRA":"Traverse", "VLA":"Villa","VLGE":"Village"}
colonnes_cadastre = ['geometry','id','commune','prefixe','section','numero']
noms_colonnes_cadastre = {'id' :'parcelId',
                            'commune': 'inseeCode',
                            'prefixe' : 'prefix',
                            'numero' :'planNumber'}
noms_colonnes_dvf = {'Date mutation' :'txDate', 'Nature mutation': 'txType', 'Valeur fonciere': 'price',  'No voie': 'streetNumber','B/T/Q': 'streetSuffix',
                        'Type de voie': 'streetType',  'Voie': 'streetName',  'Code postal': 'postCode','Commune': 'cityName',
                        'codeDepartement_codeCommuneSeule' : 'inseeCode',
                        'Code type local': 'realtyType', 'Surface reelle bati': 'livingArea','Nombre pieces principales': 'rooms', 'Nature culture': 'landType',
                        'Surface terrain': 'landArea', 'id_parcelle':'parcelId','Prefixe de section' :'prefix','Section': 'section',
                        'No plan': 'planNumber'}       
colonnes_dvf = ['Date mutation','Nature mutation','Valeur fonciere','No voie','B/T/Q','Type de voie',
           'Voie','Code postal','Code departement','Commune', 'codeDepartement_codeCommuneSeule',
           'Code type local','Surface reelle bati','Nombre pieces principales',
           'Nature culture','Surface terrain','id_parcelle','idMutation',"Prefixe de section", "Section","No plan"]
colonnes_adresse = ['numero','rep','nom_voie','code_postal','code_insee','nom_commune','lon','lat']
noms_colonnes_adresse =  {'numero': 'streetNumber',
                            'rep' :'streetSuffix',
                            'code_postal': 'postCode',
                            'Code departement' : 'departmentCode',
                            'nom_voie' : 'streetName',
                            'code_insee' :'inseeCode',
                            'nom_commune' :'cityName',
                            'lon' :'longitude',
                            'lat' :'latitude'}  
colonnes_adresse_locaux = ['N° voirie (Adresse du local)','Indice de répétition (Adresse du local)','Nature voie (Adresse du local)',
                            'Nom voie (Adresse du local)','Code Commune (Champ géographique)','Nom Commune (Champ géographique)','Département (Champ géographique)','Préfixe (Références cadastrales)','Section (Références cadastrales)','N° plan (Références cadastrales)']
noms_colonnes_adresse_locaux  = {'N° voirie (Adresse du local)':'streetNumber',
                            'Indice de répétition (Adresse du local)':'streetSuffix',
                            'Nature voie (Adresse du local)': 'streetType',
                            'Nom voie (Adresse du local)' : 'streetName',
                            'Code Commune (Champ géographique)':'inseeCode',
                            'Nom Commune (Champ géographique)':'cityName',
                            'Département (Champ géographique)':'departement', 
                            'Préfixe (Références cadastrales)':'prefixeSection', 
                            'Section (Références cadastrales)':'section', 
                            'N° plan (Références cadastrales)':'numeroPlan'} 
colonnes_adresse_cadastre = ['numero','suffixe','voie_nom','code_postal','commune_code','commune_nom','long','lat']
noms_colonnes_adresse_cadastre = {'numero': 'streetNumber',
                            'suffixe' :'streetSuffix',
                            'code_postal': 'postCode',
                            'voie_nom' : 'streetName',
                            'commune_code' :'inseeCode',
                            'commune_nom' :'cityName',
                            'long' :'longitude',
                            'lat' :'latitude'} 
liste_accepte = ["ALL","Allee","AV","Avenue","BD","Boulevard","CAR","Carrefour", "CHE","Chemin",
            "CHS","Chaussee","CITE","Cite","COR","Corniche","CRS","Cours", "DOM","Domaine","DSC","Descente",
            "ECA","Ecart","ESP","Esplanade","FG","Faubourg", "GR","Grande Rue","HAM","Hameau","HLE","Halle",
            "IMP","Impasse","LD","Lieu-dit",  "LOT","Lotissement","MAR","Marche", "MTE","Montee",
            "PAS","Passage","PL","Place", "PLN","Plaine","PLT","Plateau", "PRO","Promenade", "PRV","Parvis",
            "PTR","petite rue","QUA","Quartier", "QUAI","Quai","RES","Residence", "RLE","Ruelle",
            "ROC","Rocade", "RPT","Rond-point",  "RTE","Route","RUE","Rue", "SEN","Sente","SEN","Sentier",
            "SQ","Square", "TPL","Terre-plein", "TRA","Traverse", "VLA","Villa","VLGE","Village"]
jointure_sur_adresse = ['streetNumber', 'streetName', 'inseeCode', 'cityName','streetSuffix','streetType','postCode']
dvf_mega_drop = ['streetNumber_x','streetSuffix_x','streetType_x','streetName_x','postCode_x','cityName_x','inseeCode_x','streetNumber_y','streetSuffix_y','streetType_y','streetName_y','cityName_y','inseeCode_y','postCode_y']
liste_accepte_lower = [x.lower() for x in liste_accepte]
liste_accepte_upper = [x.upper() for x in liste_accepte]

jour = "2022-05-02"
path_jour = "D:/immo-data/jeux_donnees/" + jour
path_cadastres = "D:/immo-data/jeux_donnees/"+ jour + "/cadastres"
path_adresses = "D:/immo-data/jeux_donnees/"+ jour + "/adresses"
path_adresses_cadastre = "D:/immo-data/jeux_donnees/"+ jour + "/adresses_extraites"
path_dvf = "D:/immo-data/jeux_donnees/"+ jour + "/dvf"
path_locaux = "D:/immo-data/jeux_donnees/"+ jour + "/Fichier_des_locaux"

os.chdir(path_jour)
folder_master= 'master_dvf_test'
os.makedirs(folder_master)
path_master = path_jour + '/' + folder_master

#creation des listes de fichiers
def find_csv_filenames(path_to_dir, suffix ):
    filenames = listdir(path_to_dir)
    return [ filename for filename in filenames if filename.endswith( suffix ) ]
liste_cadastres = find_csv_filenames(path_cadastres,".gz")
liste_dvf = find_csv_filenames(path_dvf,"csv")
liste_adresses = find_csv_filenames(path_adresses,".gz")
liste_adresses_cadastre = find_csv_filenames(path_adresses_cadastre,".gz")
liste_locaux = find_csv_filenames(path_locaux,".txt")

def list_remove(list_name, v1, v2, v3):
    list_name.remove(v1)
    list_name.remove(v2)
    list_name.remove(v3)


list_remove(liste_cadastres,"cadastre_57.json.gz","cadastre_67.json.gz","cadastre_68.json.gz")
list_remove(liste_dvf,"dvf_57.csv","dvf_67.csv","dvf_68.csv")
list_remove(liste_adresses,"adresses_57.csv.gz","adresses_67.csv.gz","adresses_68.csv.gz")
list_remove(liste_adresses_cadastre,"adresses_extraites_57.csv.gz","adresses_extraites_67.csv.gz","adresses_extraites_68.csv.gz")
list_remove(liste_locaux,"PM_21_B_57.txt","PM_21_B_67.txt","PM_21_B_68.txt")


def jointure_fichiers():

    for i in range(len(liste_dvf)):

        
        # import des fichiers
        adresse_locaux = path_locaux + '/' + str(liste_locaux[i])
        df_adresse_locaux = pd.read_csv(adresse_locaux, encoding='latin-1', sep=";", low_memory=False)
        adresse_cadastre = path_adresses_cadastre + '/' + str(liste_adresses_cadastre[i])
        df_adresse_cadastre = pd.read_csv(adresse_cadastre, compression='gzip', sep=";", low_memory=False)
        adresse_normal = path_adresses + '/' + str(liste_adresses[i])
        df_adresse_normal =  pd.read_csv(adresse_normal, compression='gzip', sep=";", low_memory=False)
        dvf = path_dvf + '/' + str(liste_dvf[i])
        dvf =  pd.read_csv(dvf,low_memory=False)

        cadastre = path_cadastres + '/' + str(liste_cadastres[i])
        cadastre =  pd.read_json(cadastre, orient='str', compression='gzip')

        #traitement DF adresse
        df_adresse_normal = df_adresse_normal[colonnes_adresse]
        df_adresse_normal.rename(noms_colonnes_adresse,
                                    axis=1,
                                    inplace=True)
        df_adresse_normal['parcelId'] = np.nan
        df_adresse_normal['streetNumber'] = df_adresse_normal['streetNumber'].apply(str)
        df_adresse_normal['streetSuffix'] = df_adresse_normal['streetSuffix'].str.normalize('NFKD').str.encode('ascii', errors='ignore').str.decode('utf-8')
        df_adresse_normal['streetSuffix'] = df_adresse_normal['streetSuffix'].str.upper()
        df_adresse_normal['streetName'] = df_adresse_normal['streetName'].str.normalize('NFKD').str.encode('ascii', errors='ignore').str.decode('utf-8')
        df_adresse_normal['streetName'] = df_adresse_normal['streetName'].str.upper()
        df_adresse_normal['cityName'] = df_adresse_normal['cityName'].str.normalize('NFKD').str.encode('ascii', errors='ignore').str.decode('utf-8')
        df_adresse_normal['cityName'] = df_adresse_normal['cityName'].str.upper()
        df_adresse_normal['inseeCode'] = df_adresse_normal['inseeCode'].apply(str)
        df_adresse_normal['postCode'] = df_adresse_normal['postCode'].apply(str)
        df_adresse_normal['streetType'] = df_adresse_normal['streetName'].apply(lambda x: x.split(' ')[0] if x.split(' ')[0] in liste_accepte_upper else np.nan)
        df_adresse_normal = df_adresse_normal[['streetNumber', 'streetSuffix', 'streetType', 'streetName', 'cityName','inseeCode','postCode','parcelId','longitude','latitude']]

        # traitement DF adresses issues du cadastre
        df_adresse_cadastre = df_adresse_cadastre[colonnes_adresse_cadastre]
        df_adresse_cadastre.rename(noms_colonnes_adresse_cadastre,
                                    axis=1,
                                    inplace=True)
        df_adresse_cadastre['parcelId'] = np.nan
        df_adresse_cadastre['streetNumber'] = df_adresse_cadastre['streetNumber'].apply(str)
        df_adresse_cadastre['streetSuffix'] = df_adresse_cadastre['streetSuffix'].str.normalize('NFKD').str.encode('ascii', errors='ignore').str.decode('utf-8')
        df_adresse_cadastre['streetSuffix'] = df_adresse_cadastre['streetSuffix'].str.upper()
        df_adresse_cadastre['streetName'] = df_adresse_cadastre['streetName'].apply(str)
        df_adresse_cadastre['streetName'] = df_adresse_cadastre['streetName'].str.normalize('NFKD').str.encode('ascii', errors='ignore').str.decode('utf-8')
        df_adresse_cadastre['streetName'] = df_adresse_cadastre['streetName'].str.upper()
        df_adresse_cadastre['cityName'] = df_adresse_cadastre['cityName'].str.normalize('NFKD').str.encode('ascii', errors='ignore').str.decode('utf-8')
        df_adresse_cadastre['cityName'] = df_adresse_cadastre['cityName'].str.upper()
        df_adresse_cadastre['inseeCode'] = df_adresse_cadastre['inseeCode'].apply(str)
        df_adresse_cadastre['postCode'] = df_adresse_cadastre['postCode'].apply(str)
        df_adresse_cadastre['streetType'] = df_adresse_cadastre['streetName'].apply(lambda x: x.split(' ')[0] if x.split(' ')[0] in liste_accepte_upper else np.nan)
        df_adresse_cadastre = df_adresse_cadastre[['streetNumber', 'streetSuffix', 'streetType', 'streetName', 'cityName','inseeCode','postCode','parcelId','longitude','latitude']]
        
        # traitement adresses des locaux
        df_adresse_locaux = df_adresse_locaux[colonnes_adresse_locaux]
        df_adresse_locaux.rename(noms_colonnes_adresse_locaux,
                                    axis=1,
                                    inplace=True)
        df_adresse_locaux.prefixeSection.replace('   ', '', inplace = True)
        df_adresse_locaux['departement'] = df_adresse_locaux['departement'].apply(str)
        df_adresse_locaux['inseeCode'] = df_adresse_locaux['inseeCode'].apply(str)
        df_adresse_locaux['numeroPlan'] = df_adresse_locaux['numeroPlan'].apply(str)
        df_adresse_locaux['section'] = df_adresse_locaux['section'].str.pad(width=2, side='left', fillchar='0', )
        df_adresse_locaux['numeroPlan'] = df_adresse_locaux['numeroPlan'].str.pad(width=4, side='left', fillchar='0', )
        df_adresse_locaux['prefixeSection'] = df_adresse_locaux['prefixeSection'].str.pad(width=3, side='left', fillchar='0', )
        df_om = df_adresse_locaux[(df_adresse_locaux['departement'] == '971') 
                | (df_adresse_locaux['departement'] == '972')
                | (df_adresse_locaux['departement'] == '973')
                | (df_adresse_locaux['departement'] == '974')]
        df_om['code_Commune_Seule'] = df_om['inseeCode'].str.pad(width=2, side='left', fillchar='0', )
        df_om['codeDepartement_codeCommuneSeule'] = df_om['departement'] + df_om['code_Commune_Seule']
        df_pas_om = df_adresse_locaux[(df_adresse_locaux['departement'] != '971') 
                & (df_adresse_locaux['departement'] != '972')
                & (df_adresse_locaux['departement'] != '973')
                & (df_adresse_locaux['departement'] != '974')]
        df_pas_om['departement'] = df_pas_om['departement'].str.pad(width=2, side='left', fillchar='0', )
        df_pas_om['code_Commune_Seule'] = df_pas_om['inseeCode'].str.pad(width=3, side='left', fillchar='0', )
        df_pas_om['codeDepartement_codeCommuneSeule'] = df_pas_om['departement'] + df_pas_om['code_Commune_Seule']
        df_adresse_locaux = pd.concat([df_om, df_pas_om])
        df_adresse_locaux['parcelId'] = df_adresse_locaux['codeDepartement_codeCommuneSeule'] + df_adresse_locaux['prefixeSection'] + df_adresse_locaux['section'] + df_adresse_locaux['numeroPlan'] 
        df_adresse_locaux.drop(['code_Commune_Seule','departement','prefixeSection','section','numeroPlan','codeDepartement_codeCommuneSeule'],
                        axis = 1,
                        inplace = True)
        df_adresse_locaux['postCode'] = np.nan
        df_adresse_locaux['longitude'] = np.nan
        df_adresse_locaux['latitude'] = np.nan
        df_adresse_locaux = df_adresse_locaux[['streetNumber', 'streetSuffix', 'streetType', 'streetName', 'cityName','inseeCode','postCode','parcelId','longitude','latitude']]
        df_adresse_locaux['streetNumber'] = df_adresse_locaux['streetNumber'].apply(str)
        df_adresse_locaux['streetNumber'] = df_adresse_locaux['streetNumber'].str.replace('.0', '')
        df_adresse_locaux['streetSuffix'] = df_adresse_locaux['streetSuffix'].str.normalize('NFKD').str.encode('ascii', errors='ignore').str.decode('utf-8')
        df_adresse_locaux['streetSuffix'] = df_adresse_locaux['streetSuffix'].str.upper()
        df_adresse_locaux['streetType'] = df_adresse_locaux['streetType'].str.normalize('NFKD').str.encode('ascii', errors='ignore').str.decode('utf-8')
        df_adresse_locaux['streetType'] = df_adresse_locaux['streetType'].str.upper()
        df_adresse_locaux['streetName'] = df_adresse_locaux['streetName'].str.normalize('NFKD').str.encode('ascii', errors='ignore').str.decode('utf-8')
        df_adresse_locaux['streetName'] = df_adresse_locaux['streetName'].str.upper()
        df_adresse_locaux['cityName'] = df_adresse_locaux['cityName'].str.normalize('NFKD').str.encode('ascii', errors='ignore').str.decode('utf-8')
        df_adresse_locaux['cityName'] = df_adresse_locaux['cityName'].str.upper()

        # traitement dvf
        dvf = dvf[colonnes_dvf]
        dvf.rename(noms_colonnes_dvf,
                    axis=1,
                    inplace=True)
        dvf['streetNumber'] = dvf['streetNumber'].apply(str)
        dvf['streetNumber'] = dvf['streetNumber'].str.replace('.0', '')
        dvf['streetSuffix'] = dvf['streetSuffix'].str.normalize('NFKD').str.encode('ascii', errors='ignore').str.decode('utf-8')
        dvf['streetSuffix'] = dvf['streetSuffix'].str.upper()
        dvf['streetName'] = dvf['streetName'].str.normalize('NFKD').str.encode('ascii', errors='ignore').str.decode('utf-8')
        dvf['streetName'] = dvf['streetName'].str.upper()
        dvf['cityName'] = dvf['cityName'].str.normalize('NFKD').str.encode('ascii', errors='ignore').str.decode('utf-8')
        dvf['cityName'] = dvf['cityName'].str.upper()
        dvf['inseeCode'] = dvf['inseeCode'].apply(str)
        dvf['postCode'] = dvf['postCode'].apply(str)
        dvf['postCode'] = dvf.apply(lambda x: x['postCode'][:-2], axis = 1)
        dvf['realtyType'] = dvf['realtyType'].fillna(0)

        # traitement cadastre
        cadastrePolygons = cadastre["features"]
        cadastre = gpd.GeoDataFrame.from_features(cadastrePolygons)

        cadastre = cadastre[colonnes_cadastre]
        cadastre.rename(noms_colonnes_cadastre,
                        axis=1,
                        inplace=True)
        cadastre.drop(['planNumber','prefix','section','inseeCode'],axis = 1, inplace = True)

        # creation du datafarame des 3 sources d'adresses
        mega_adresse = pd.concat([df_adresse_cadastre, df_adresse_locaux, df_adresse_normal]).drop_duplicates()

        #dvf + adresse avec jointure_sur_adresse
        dvf_mega = pd.merge(dvf, mega_adresse , on = jointure_sur_adresse, how="left")
        dvf_mega['parcelId_x'] = dvf_mega['parcelId_x'].fillna(dvf_mega["parcelId_y"])
        dvf_mega.drop('parcelId_y',axis = 1,inplace = True)
        dvf_mega.rename({'parcelId_x': 'parcelId'},axis = 1, inplace = True)

        #dvf + cadastre sur id
        dvf_mega = pd.merge(dvf_mega, cadastre , on = 'parcelId', how="left")
        dvf_mega.rename({'geometry': 'geometryFromId'},axis = 1, inplace = True)

        #dvf + cadastre sur geo sjoin
        dvf_mega = gpd.GeoDataFrame(dvf_mega, geometry= gpd.points_from_xy(dvf_mega.longitude, dvf_mega.latitude))
        polygons_contains = dvf_mega.sjoin(cadastre, how="left", predicate='within')
        polygons_contains['parcelId_left'] = polygons_contains['parcelId_left'].fillna(polygons_contains["parcelId_right"])
        polygons_contains.drop('parcelId_right',axis = 1,inplace = True)
        polygons_contains.rename({'parcelId_left': 'parcelId'},axis = 1, inplace = True)
        polygons_contains.rename({'geometry': 'geometryFromSjoin'},axis = 1, inplace = True)

        # ajout centroids
        polygons_contains['longitude_dup'] = polygons_contains['geometryFromId'].centroid.y
        polygons_contains['latitude_dup'] = polygons_contains['geometryFromId'].centroid.x
        polygons_contains['longitude'] = polygons_contains['longitude'].fillna(polygons_contains["longitude_dup"])
        polygons_contains['latitude'] = polygons_contains['latitude'].fillna(polygons_contains["latitude_dup"])
        polygons_contains.drop(["index_right","longitude_dup","latitude_dup","geometryFromSjoin"],axis = 1, inplace = True)
        polygons_contains.rename(columns={"geometryFromId": "geometry"}, inplace = True)

        #séparation en communes
        labels = polygons_contains['inseeCode'].unique()

        for label in labels:
            df_commune =polygons_contains.loc[polygons_contains['inseeCode']==label,:]
            path_dvf_master = path_master  +  "/" + label + ".csv"
            df_commune.to_csv(path_dvf_master,index = False)
        
        # ajout des lignes n'ayant pas de parcelle dans un DF pour exploration
        df_tampon_manquant = polygons_contains[polygons_contains['parcelId'].isnull()]
        global df_manquants
        frames = [df_manquants, df_tampon_manquant]
        df_manquants = pd.concat(frames)
    
jointure_fichiers()

#creation fichier avec les parcelles manquantes
path_manquant = path_jour + '/parcelId_manquant_dvf.csv'
df_manquants.to_csv(path_manquant, index= False)

# cd D:/immo-data/code
# python jointure_commune_26_04.py